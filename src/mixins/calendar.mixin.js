import randomKey from "@/untils/randomKey";
export default {
    methods: {
        addClassToday(date) {
            if (
                new Date(date.setHours(0, 0, 0, 0)).getTime() ===
                new Date().setHours(0, 0, 0, 0)
            )
                return "today";
            return "";
        },

        convertTwoDigitNumber(value) {
            return value < 10 ? '0' + value : value
        },

        setFontWeigthBold(hour) {
            const nowHour = new Date().toLocaleTimeString().split(":")[0];
            if (hour.split(":")[0] == nowHour) return "bold";
        },

        setHeigthEvent(startTime, endTime) {
            const first = new Date(`Jan 01, 2000 ${startTime}:00`).getTime();
            const second = new Date(
                `Jan 01, 2000 ${endTime == "00:00" ? "24:00" : endTime}:00`
            ).getTime();
            const differenceInMinutes = (second - first) / 1000 / 60;
            return 48 * (differenceInMinutes / 60) + "px";
        },

        setNullHours(date) {
            return new Date(new Date(date).setHours(0, 0, 0, 0)).getTime();
        },

        setLineParemetrs(element) {
            const nowDate =
                ((60 * new Date().getHours() + new Date().getMinutes()) /
                    1440) *
                1151 +
                48;
            element.style.top = nowDate.toFixed(2) + "px";
        },

        setTopEvent(startTime) {
            return 48 * (startTime.split(":")[1] / 60) + "px";
        },

        showEvent(hour, date, eventArr) {
            return eventArr
                .filter(
                    (itm) =>
                    this.setNullHours(itm.date) == date.getTime() &&
                    this.checTwoHours(hour, itm.startTime)
                )
                .map((itm) => {
                    return {
                        ...itm,
                        height: this.setHeigthEvent(itm.startTime, itm.endTime),
                        top: this.setTopEvent(itm.startTime),
                        key: randomKey(),
                    };
                });
        },

        checTwoHours(hour, startTime) {
            if (hour.split(":")[0] == startTime.split(":")[0]) return true;
            return false;
        },

    }
}