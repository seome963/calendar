import Vue from 'vue'
import App from './App.vue'
import './registerServiceWorker'
import store from './store'
import randomKey from '@/untils/randomKey'

Vue.config.productionTip = false
Vue.use(randomKey)
new Vue({
  store,
  render: h => h(App)
}).$mount('#app')